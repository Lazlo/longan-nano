# Longan Nano Experiments

Firmware for the [Longan Nano](https://www.seeedstudio.com/Sipeed-Longan-Nano-RISC-V-GD32VF103CBT6-Development-Board-p-4205.html) RISC-V board.

## Checkout

```
git clone git@gitlab.com:Lazlo/longan-nano.git
cd longan-nano
git submodule update --init --recursive
```

## Hardware

 * **CPU**: GigaDevice GD32VF103CBT6 RISC-V 32-bit core
   - **Memories**: 128KB Flash, 32KB SRAM
 * **Crystal**: 8MHz Passive High Speed Crystal, 32.768KHz Low Speed RTC Crystal
 * **Display**: 160x80 RGB IPS LCD (SPI Interface)

## Flashing

There are three ways to write firmware to the flash of the board.

 * Serial
 * Device Firmware Upgrade (DFU) via USB-C
 * JTAG

To activate serial upload or DFU hold the ```BOOT``` key, then press the ```RESET``` key. This sequence activates DFU.

## Official Resources

 * Seeed [Shop](https://www.seeedstudio.com/Sipeed-Longan-Nano-RISC-V-GD32VF103CBT6-Development-Board-p-4205.html)
 * Sipeed [Wiki](http://longan.sipeed.com/en/)
   - [Firmware Examples](https://github.com/sipeed/Longan_GD32VF_examples) for the Longan Nano
 * GigaDevice [Documents and Various Sources](https://gd32mcu.21ic.com/en/index)
